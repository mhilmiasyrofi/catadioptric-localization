#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>

using namespace cv;
using namespace std;

#define RADIUS 250

bool inRange(int x, int min, int max)
{
    return x >= min && x <= max;
}

int main(int argc, char **argv)
{
    if (argc != 2)
    {
        cout << " Usage: display_image ImageToLoadAndDisplay" << endl;
        return -1;
    }

    Mat image;
    image = imread(argv[1], CV_LOAD_IMAGE_COLOR); // Read the file

    Mat dest(image.size().height, image.size().width, CV_8UC3, Scalar(0, 0, 0));

    if (!image.data) // Check for invalid input
    {
        cout << "Could not open or find the image" << std::endl;
        return -1;
    }

    Point center(image.size().width/2, image.size().height/2);

    // Calculate all points on that circle
    // Axes are half of the ellipse axes, a circle is a perfect ellipse, with radius = 2x half of the axes :)
    Size axes(RADIUS, RADIUS);
    vector<Point> circle_points;
    ellipse2Poly(center, axes, 0, 0, 360, 1, circle_points);



    for (int i= 0; i < circle_points.size(); i++) {
        Vec3b val = image.at<Vec3b>(circle_points[i]);
        val[0] = 255;
        val[1] = 255;
        val[2] = 255;
        dest.at<Vec3b>(circle_points[i]) = val;

        // draw line from center to edge
        // from 8-bit 3-channel image to the buffer
        Point edge_circle(circle_points[i]);
        LineIterator it(image, edge_circle, center, 8);

        vector<Vec3b> buf(it.count);
        // alternative way of iterating through the line
        for (int i = 0; i < it.count; i++, ++it) {
            Vec3b val = image.at<Vec3b>(it.pos());
            if (inRange(val[0], 100, 255) && inRange(val[1], 100, 255) && inRange(val[2], 100, 255)) {
                dest.at<Vec3b>(it.pos()) = Vec3b(255, 255, 255);
            } else if (inRange(val[0], 0, 70) && inRange(val[1], 0, 250) && inRange(val[2], 100, 255)) {
                dest.at<Vec3b>(it.pos()) = Vec3b(0, 0, 170);
            } else {
                dest.at<Vec3b>(it.pos()) = Vec3b(0, 100, 0);;
            }
            


        }
    }

    cout << image.at<Vec3b>(Point(482, 366)) << endl;

    namedWindow("Display window", WINDOW_AUTOSIZE); // Create a window for display.
    imshow("Display window", image);                // Show our image inside it.

    namedWindow("Display Result", WINDOW_AUTOSIZE); // Create a window for display.
    imshow("Display Result", dest);                // Show our image inside it.

    waitKey(0); // Wait for a keystroke in the window
    return 0;
}